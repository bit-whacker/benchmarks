1. The folder name is the scientific organism;

2. The txt file under the folder contains the mapping (between INSDC and Swiss-Prot) cases where duplicates occur

3. For each row, the format is:
Protein record accession: cross-referenced nucleotide record 1, cross-referenced nucleotide record 2...

4. Let's see an example from the first line from Arabidopsis Thaliana file: 
Q9FNX5:AF488725 AF180733 AJ304842 CP002686 AL138658 AF428332

The first is a Swiss-Prot protein record with accession number Q9FNX5 (http://www.uniprot.org/uniprot/Q9FNX5).
The rest is the cross-referenced INSDC nucleotide records, e.g., record with accession number AF488725
(https://www.ncbi.nlm.nih.gov/nuccore/AF488725). Those nucleotide records point to the same protein record
and are explicitly cross-referenced via expert curation.

When developing duplicate detection or record linkage method, it should be tested without giving the mapping, 
i.e., without providing the protein record. For instance, compare nucleotide records pairwise in terms of meta
data and sequence similarity. The benchmarks should be used for duplicate detection within nucleotide databases.

